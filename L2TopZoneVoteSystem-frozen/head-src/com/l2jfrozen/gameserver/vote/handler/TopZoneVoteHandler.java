package com.l2jfrozen.gameserver.vote.handler;

import org.codehaus.jackson.map.ObjectMapper;

import com.l2jfrozen.Config;
import com.l2jfrozen.gameserver.vote.AbstractVoteManager;
import com.l2jfrozen.gameserver.vote.model.json.TopZoneGlobalResponse;
import com.l2jfrozen.gameserver.vote.model.json.TopZonePersonalResponse;

/**
 * @author L2Cygnus
 */
public class TopZoneVoteHandler extends AbstractVoteManager implements IVoteHandler {

	@Override
	public boolean isVoted(String ip) {
		String url = getApiUrlTemplatePersonal(ip);
		String response = getResponse(url, "L2TopZone");
		TopZonePersonalResponse resp = null;
		boolean voted = false;
		try {
			resp = new ObjectMapper().readValue(response, TopZonePersonalResponse.class);
			if (resp != null) {
				voted = resp.getResult().isVoted();
			}
		} catch (Exception e) {
			if (Config.VOTE_LOG_ERROR) {
				e.printStackTrace();
			}
			_log.warning("Failed to check personal vote status for topzone");
		}
		return voted;
	}

	@Override
	public int getTotalVotes() {
		String url = getApiUrlTemplateTotal();
		String response = getResponse(url, "L2TopZone");
		int votes = -1;
		TopZoneGlobalResponse jsonResponse = null;
		try {
			jsonResponse = new ObjectMapper().readValue(response, TopZoneGlobalResponse.class);
			if (jsonResponse != null) {
				votes = jsonResponse.getResult().getTotalVotes();
			}
		} catch (Exception e) {
			if (Config.VOTE_LOG_ERROR) {
				e.printStackTrace();
			}
			_log.warning("Failed to read total votes for topzone");
		}
		return votes;
	}

	private String getApiUrlTemplatePersonal(String ip) {
		return String.format("%svote?token=%s&ip=%s", Config.VOTE_TOPZONE_API_URL, Config.VOTE_TOP_ZONE_TOKEN, ip);
	}

	private String getApiUrlTemplateTotal() {
		return String.format("%sserver_%s/getServerData", Config.VOTE_TOPZONE_API_URL, Config.VOTE_TOP_ZONE_TOKEN);
	}
}
